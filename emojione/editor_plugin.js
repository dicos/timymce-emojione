(function() {
	tinymce.create('tinymce.plugins.EmojiOnePlugin', {
		
		init : function(ed, url) {
		// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');

			ed.addCommand('EmojiOneCommand', function() {
				ed.windowManager.open({
					file : url + '/popup.html',
					width : 630,
					height : 350,
					inline : 1
				}, {
					plugin_url : url // Plugin absolute URL
				});
			});

			// Register example button
			ed.addButton('emojione', {
				title : 'Emoji One',
				cmd : 'EmojiOneCommand',
				image : url + '/icons/1F600.png'
			});
			
			// Add a node change handler, selects the button in the UI when a image is selected
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('EmojiOnePlugin', n.nodeName == 'IMG');
			});
		},
		createControl : function(n, cm) {
			return null;
		},
		getInfo : function() {
			return {
					longname  : 'wp_emoji_one',
					author 	  : 'Monchito.net',
					authorurl : 'http://www.monchito.net',
					infourl   : 'http://labs.monchito.net/wp-emoji-one/',
					version   : "0.5.0"
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('emojione', tinymce.plugins.EmojiOnePlugin);
})(tinymce);

